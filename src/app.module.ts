import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CorreosModule } from './correos/correos.module';
import { GruposModule } from './grupos/grupos.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Correo } from './correos/entities/correo.entity';
import { CorreosGruposModule } from './correos_grupos/correos_grupos.module';
import { CorreosGrupo } from './correos_grupos/entities/correos_grupo.entity';
import { Grupo } from './grupos/entities/grupo.entity';
import { MailModule } from './mail/mail.module';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';


@Module({
  imports: [ConfigModule.forRoot({
    envFilePath: [".env", ".env.development"],
    isGlobal: true
  }), CorreosModule, GruposModule, CorreosGruposModule, UsersModule, AuthModule, MailModule , TypeOrmModule.forRoot({
    type: "mysql",
    host: process.env.DATABASE_HOST,
    port:  parseInt(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    entities: [Correo, CorreosGrupo, Grupo, User],
    synchronize: true,
    database: "envio-correos"
  }), TypeOrmModule.forFeature([Correo, CorreosGrupo, Grupo, User])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
