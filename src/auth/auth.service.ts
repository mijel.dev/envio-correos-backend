import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { UsersService } from 'src/users/users.service';
import * as bcryptjs from "bcryptjs";
import { JwtService } from "@nestjs/jwt";
import { User } from 'src/users/entities/user.entity';
import { RegisterDto } from './dto/register.dto';



@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService
    ) { }

    async register({ password, email, name}: RegisterDto) : Promise<any> {
        try {
            const user = await this.userService.findOneByEmail(email)

            if (user[0]) {
                throw new BadRequestException("Email already exists");
            }

            const hashedPassword = await bcryptjs.hash(password, 10)
            await this.userService.create({
                name,
                email,
                password: hashedPassword
            })

            return {
                message: "Usuario creado con éxito"
            }

        } catch (error) {
            console.error('Error al crear usuario:', error);
            throw new Error('Error al crear usuario');
        }
    }

    async login(loginDto: LoginDto): Promise<any> {

        try {
            const { email, password } = loginDto

            const user = await this.userService.findOneByEmail(email);

            if (!user) {
                throw new UnauthorizedException("Invalid email");
            }

            const isPasswordValid = await bcryptjs.compare(password, user[0].password);

            if (!isPasswordValid) {
                throw new UnauthorizedException("Invalid password");
            }

            const payload = { email: user[0].email }

            const token = await this.jwtService.signAsync(payload)

            return {
                email: user[0].email,
                token
            };
        } catch (error) {
            console.error('Error al realizar la autenticación:', error);
            throw new Error('Error al realizar la autenticación');
        }
    }
}
