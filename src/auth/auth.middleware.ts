import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private readonly jwtService: JwtService
  ){}

  use(req: any, res: any, next: () => void) {
    const bearerHeader = req.headers.authorization;

    if (!bearerHeader) {
      throw new UnauthorizedException("Token de acceso no proporcionado.")
    }

    const token = bearerHeader.split(' ')[1];

    try {
      const decode = this.jwtService.verify(token) as any;
      req.user = decode;
      next();
    } catch(error) {
      throw new UnauthorizedException('Token de acceso inválido'); 
    }
  }
}
