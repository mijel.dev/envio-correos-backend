import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { ValidationPipe } from '@nestjs/common';

dotenv.config();

async function bootstrap() {
  // dotenv.config()
  const app = await NestFactory.create(AppModule);

  // app.enableCors({
  //   origin: [
  //     "http://localhost:5173/",
  //   ],
  // });

  app.enableCors({
    origin: 'http://localhost:5173', // Reemplaza con la URL de tu frontend en Svelte
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
  });

  app.useGlobalPipes(new ValidationPipe());

  await app.listen(3000);
}
bootstrap();
