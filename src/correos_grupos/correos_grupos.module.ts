import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CorreosGruposService } from './correos_grupos.service';
import { CorreosGruposController } from './correos_grupos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CorreosGrupo } from './entities/correos_grupo.entity';
import { Correo } from 'src/correos/entities/correo.entity';
import { Grupo } from 'src/grupos/entities/grupo.entity';
import { AuthMiddleware } from 'src/auth/auth.middleware';

@Module({
  imports: [
    TypeOrmModule.forFeature([CorreosGrupo]),
  ],
  controllers: [CorreosGruposController],
  providers: [CorreosGruposService],
})
export class CorreosGruposModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(CorreosGruposController)
  }
}
