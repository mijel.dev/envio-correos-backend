import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CorreosGruposService } from './correos_grupos.service';
import { CreateCorreosGrupoDto } from './dto/create-correos_grupo.dto';
import { UpdateCorreosGrupoDto } from './dto/update-correos_grupo.dto';

@Controller('correos-grupos')
export class CorreosGruposController {
  constructor(private readonly correosGruposService: CorreosGruposService) {}

  // Crear un nuevo grupo con correos
  @Post()
  create(@Body() createCorreosGrupoDto: CreateCorreosGrupoDto) {
    return this.correosGruposService.create(createCorreosGrupoDto);
  }

  // Obtener todos los correos relacionados al grupo
  @Get()
  findAll() {
    return this.correosGruposService.findAll();
  }
}
