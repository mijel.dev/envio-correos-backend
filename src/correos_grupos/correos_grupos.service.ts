import { Injectable } from '@nestjs/common';
import { CreateCorreosGrupoDto } from './dto/create-correos_grupo.dto';
import { UpdateCorreosGrupoDto } from './dto/update-correos_grupo.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CorreosGrupo } from './entities/correos_grupo.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CorreosGruposService {
  constructor(
    @InjectRepository(CorreosGrupo)
    private readonly correoGrupoRepository: Repository<CorreosGrupo>,
  ) { }

  create(createCorreosGrupoDto: CreateCorreosGrupoDto) {
    try {
      const nuevoCorreoGrupo = this.correoGrupoRepository.create(createCorreosGrupoDto);
      return this.correoGrupoRepository.save(nuevoCorreoGrupo);
    } catch (error) {
      console.error("Error al crear una relacipón grupo - correo: ", error)
      throw new Error("Error al crear una relacipón grupo - correo")
    }
  }

  async findAll() {

    try {
      const resultado = await this.correoGrupoRepository
        .createQueryBuilder('correoGrupo')
        .innerJoinAndSelect('correoGrupo.usuario', 'usuario')
        .innerJoinAndSelect('correoGrupo.grupo', 'grupo')
        .getMany();

      return resultado.map(({ usuario, grupo }) => ({
        correo: {
          id: usuario.id,
          name: usuario.name,
          email: usuario.email,
          number: usuario.number,
          active: usuario.active
        },
        grupo: {
          id: grupo.id,
          name: grupo.name,
          description: grupo.description,
          active: grupo.active
        },
      }));
    } catch (error) {
      console.error("Error al obtener los grupos con correo: ", error)
      throw new Error("Error al obtener los grupos con correo")
    }
  }
}
