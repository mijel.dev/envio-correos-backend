import { Controller, Get, Post, Body, Patch, Param } from '@nestjs/common';
import { CorreosService } from './correos.service';
import { CreateCorreoDto } from './dto/create-correo.dto';
import { UpdateCorreoDto } from './dto/update-correo.dto';

@Controller('correos')
export class CorreosController {
  constructor(private readonly correosService: CorreosService) {}

  // Crear correo
  @Post()
  create(@Body() createCorreoDto: CreateCorreoDto) {
    return this.correosService.create(createCorreoDto);
  }

  // Traer todos los correos
  @Get()
  findAll() {
    return this.correosService.findAll();
  }

  // Obtener un correo
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.correosService.findOne(+id);
  }

  // Actualizar correo
  @Patch(':id')
  updateEmail(@Param('id') id: string, @Body() updateCorreoDto: UpdateCorreoDto) {
    return this.correosService.update(+id, updateCorreoDto);
  }
}
