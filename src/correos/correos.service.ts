import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateCorreoDto } from './dto/create-correo.dto';
import { UpdateCorreoDto } from './dto/update-correo.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Correo } from './entities/correo.entity';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';
import { MailService } from 'src/mail/mail.service';

@Injectable()
export class CorreosService {
  constructor(
    @InjectRepository(Correo)
    private readonly correoRepository: Repository<Correo>,
    private readonly mailService: MailService
  ) { }
  async create(createCorreoDto: CreateCorreoDto) {
    try {
      const errors = await validate(createCorreoDto)

      if (errors.length > 0) {
        throw new BadRequestException('Datos de correo no válidos');
      }

      const correoRepetido = await this.correoRepository.findOne({ where: { email: createCorreoDto.email } })

      if (correoRepetido) {
        throw new NotFoundException('El correo ya está registrado');
      }


      const nuevoCorreo = this.correoRepository.create(createCorreoDto)
      return this.correoRepository.save(nuevoCorreo);
    } catch (error) {
      console.error("Error al crear un correo: ", error)
      throw new Error("rror al crear un correo")
    }
  }

  async findAll() {
    try {
      const correos = this.correoRepository.findAndCount()

      this.mailService.sendEmail("mijel.dev@gmail.com", "Prueba de envio", "Contenido de prueba")

      return correos;
    } catch (error) {
      console.error("Error al obtener los correos: ", error)
      throw new Error("Error al obtener los correos")
    }
  }

  findOne(id: number) {
    try {
      const correo = this.correoRepository.findOne({ where: { id } });
      return correo;
    } catch (error) {
      console.error("Error al obtener el correo: ", error)
      throw new Error("Error al obtener el correo")
    }
  }

  async update(id: number, updateCorreoDto: UpdateCorreoDto) {
    try {
      const correo = this.correoRepository.findOne({ where: { id } })

      if (!correo) {
        throw new NotFoundException(`Correo con ID ${id} no encontrado`)
      }

      const newCorreo = this.correoRepository.update({ id }, { ...updateCorreoDto })

      return {
        message: "Correo actualizado correctamente"
      };
    } catch (error) {
      console.error("Error al actualizar el correo: ", error)
      throw new Error("Error al actualizar el correo")
    }
  }
}
