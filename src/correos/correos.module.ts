import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CorreosService } from './correos.service';
import { CorreosController } from './correos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Correo } from './entities/correo.entity';
import { CorreosGrupo } from 'src/correos_grupos/entities/correos_grupo.entity';
import { Grupo } from 'src/grupos/entities/grupo.entity';
import { MailService } from 'src/mail/mail.service';
import { AuthMiddleware } from 'src/auth/auth.middleware';

@Module({
  imports: [
    TypeOrmModule.forFeature([Correo]),
  ],
  controllers: [CorreosController],
  providers: [CorreosService, MailService],
})
export class CorreosModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(CorreosController)
  }
}
