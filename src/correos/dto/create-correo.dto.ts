// import { Is}

import { IsBoolean, IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class CreateCorreoDto {
    @IsNotEmpty()
    @IsString()
    name: string

    @IsNotEmpty()
    @IsString()
    @MinLength(9)
    number: string

    @IsNotEmpty()
    @IsString()
    @IsEmail()
    email: string;

    @IsBoolean()
    active?: boolean;
}
