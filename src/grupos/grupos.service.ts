import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateGrupoDto } from './dto/create-grupo.dto';
import { UpdateGrupoDto } from './dto/update-grupo.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Grupo } from './entities/grupo.entity';
import { In, Repository } from 'typeorm';
import { Correo } from 'src/correos/entities/correo.entity';
import { CorreosGrupo } from 'src/correos_grupos/entities/correos_grupo.entity';
import { MailService } from 'src/mail/mail.service';
import { SendEmailTo } from './dto/send-email.dto';

@Injectable()
export class GruposService {
  constructor(
    @InjectRepository(Grupo)
    private readonly grupoRepository: Repository<Grupo>,
    @InjectRepository(Correo)
    private readonly correoRepository: Repository<Correo>,
    private readonly mailService: MailService,
    @InjectRepository(CorreosGrupo)
    private readonly correoGruposRepository: Repository<CorreosGrupo>
  ) { }
  async create(createGrupoDto: CreateGrupoDto) {
    try {
      const { name, description, users, active } = createGrupoDto;

      const group = this.grupoRepository.create({
        name,
        description,
        active
      });

      await this.grupoRepository.save(group);

      const correosGrupoPromises = users.map(async (userId: number) => {
        const correo = await this.correoRepository.findOne({
          where: { id: userId }
        });

        if (correo) {
          const correosGrupo = new CorreosGrupo();
          correosGrupo.usuario = correo;
          correosGrupo.grupo = group;
          await this.correoGruposRepository.save(correosGrupo);
        }
      });

      await Promise.all(correosGrupoPromises);

      return group;
    } catch (error) {
      console.error('Error al crear el grupo y asociar usuarios:', error);
      throw new Error('Error al crear el grupo y asociar usuarios');
    }
  }

  async findAllWithCorreos(): Promise<any[]> {
    try {
      const query = `
      SELECT
        g.id AS id,
        g.name AS name,
        g.description AS description,
        g.active AS active,
        JSON_ARRAYAGG(JSON_OBJECT(
          'id', c.id,
          'name', c.name,
          'email', c.email,
          'number', c.number,
          'active', c.active
        )) AS correos
      FROM
        grupo g
        INNER JOIN correos_grupo cg ON g.id = cg.grupoId
        INNER JOIN correo c ON cg.usuarioId = c.id
      GROUP BY
        g.id
    `;

      return await this.grupoRepository.query(query);
    } catch (error) {
      console.error('Error al buscar los grupos con correos:', error);
      throw new Error('Error al buscar los grupos con correos');
    }
  }

  update(id: number, updateGrupoDto: UpdateGrupoDto) {
    try {
      const grupo = this.grupoRepository.findOne({ where: { id } })

      if (!grupo) {
        throw new NotFoundException(`Grupo con ID ${id} no encontrado`)
      }

      const newGrupo = this.grupoRepository.update({ id }, { ...updateGrupoDto })

      return {
        message: "Correo actualizado correctamente"
      };
    } catch (error) {
      console.error('Error al actualizar el grupo: ', error);
      throw new Error('Error al actualizar el grupo');
    }
  }

  async sendEmailToGroup(sendEmailDto: SendEmailTo, group_id: string) {
    try {
      const query = `
        SELECT c.*
        FROM correo c
        JOIN correos_grupo cg ON c.id = cg.usuarioId
        JOIN grupo g ON cg.grupoId = g.id
        WHERE g.id = ${group_id};
      `;

      const mails = await this.grupoRepository.query(query)

      for (const mail of mails) {
        this.mailService.sendEmail(mail.email, "Prueba de envio", sendEmailDto.content)
      }

      return { message: "Correos enviados con éxito" }
    } catch (error) {
      console.error('Error al enviar los coreos: ', error);
      throw new Error('Error al enviar los coreos');
    }
  }
}
