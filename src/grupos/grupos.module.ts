import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { GruposService } from './grupos.service';
import { GruposController } from './grupos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CorreosGrupo } from 'src/correos_grupos/entities/correos_grupo.entity';
import { Correo } from 'src/correos/entities/correo.entity';
import { Grupo } from './entities/grupo.entity';
import { MailService } from 'src/mail/mail.service';
import { AuthMiddleware } from 'src/auth/auth.middleware';

@Module({
  imports: [
    TypeOrmModule.forFeature([Grupo, Correo, CorreosGrupo]),
  ],
  controllers: [GruposController],
  providers: [GruposService, MailService],
})
export class GruposModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(GruposController)
  }
}
