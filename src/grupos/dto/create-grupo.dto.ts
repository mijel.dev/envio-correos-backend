import { IsArray, IsNotEmpty, IsString } from "class-validator";

export class CreateGrupoDto {
    readonly name: string;
    readonly description: string;
    readonly users: number[]; // IDs de los usuarios
    readonly active: boolean;
}
