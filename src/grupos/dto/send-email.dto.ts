import { IsArray, IsNotEmpty, IsString } from "class-validator";

export class SendEmailTo {
    readonly content: string;
}
