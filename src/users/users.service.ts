import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) { }
  
  async create(createUserDto: CreateUserDto) {
    try {
      return await this.userRepository.save(createUserDto);
    } catch (error) {
      console.error('Error al crear un usuario:', error);
      throw new Error('Error al crear un usuario');
    }
  }

  async findOneByEmail(email: string) {
    try {
      return await this.userRepository.findBy({ email })
    } catch (error) {
      console.error('Error al obtener un usuario por el email:', error);
      throw new Error('Error al obtener un usuario por el email');
    }
  }
}
