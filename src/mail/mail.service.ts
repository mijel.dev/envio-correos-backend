import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Correo } from 'src/correos/entities/correo.entity';


@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendEmail(destination: string, topic: string, content: string) {
    await this.mailerService.sendMail({
      to: destination,
      subject: topic,
      template: './confirmations',
      context: { 
        content
      },
    });
  }
}